# libadwaita-rs

The Rust bindings of [libadwaita](https://gitlab.gnome.org/exalm/libadwaita)

## Documentation
- libadwaita: https://bilelmoussaoui.pages.gitlab.gnome.org/libadwaita-rs/libadwaita
- libadwaita-sys: https://bilelmoussaoui.pages.gitlab.gnome.org/libadwaita-rs/libadwaita_sys